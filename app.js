var Application = (function () {
    'use strict';
    var express = require('express');
    var debug = require('debug')('abrishams:server');
    var favicon = require('serve-favicon');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var mongoose = require("mongoose");
    var lodash = require("lodash");

    var config = require('./config');
    var logger = require('./helpers/logger');
    var multer = require('./helpers/multer');

    mongoose.connect(config.get('database'));
    mongoose.connection.on('error', function (err) {
        if (err) throw err;
    });
    mongoose.connection.once('open', function () {
        debug('mongodb connected');
    });

    var app = express();

    app.set('multer', multer);
    app.set('lodash', lodash);
    app.set('debugger', debug);
    app.set('authKey', config.get('authKey'));
    app.set('view engine', 'jade');
    // use morgan to log requests to the console
    //Winston logger
    //app.use(logger); // Раскомментировать для продакшена
    app.use(logger.httpLogger('combined'));
    // uncomment after placing your favicon in /public
    //app.use(favicon(__dirname + '/public/favicon.ico'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cookieParser());

    //Набор роутеров
    require("./routes")(app);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handlers
    app.use(logger.errorLogger);
    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(logger.devErrorCallback);
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(logger.prodErrorCallback);

    return app;
})();


module.exports = Application;