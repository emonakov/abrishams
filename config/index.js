var path = require("path");
var nconf = require("nconf");

nconf.file({ file: path.join(__dirname, 'parameters.json') });

module.exports = nconf;