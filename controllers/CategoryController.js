var CategoryController = function (app) {
    'use strict';
    var util = require("util");
    var sanitizer = require("sanitizer");
    var Category = require('../models/CategoryModel');
    var Errors = require('../helpers/errors');

    var imagesTypes = ['image/jpeg', 'image/gif', 'image/png'];

    function CategoryController() {
        app.get('lodash').bindAll(this);
    }

    /**
     * Возвращает найденные категории пользователю
     * @param req
     * @param res
     * @param next
     */
    CategoryController.prototype.getAllCategories = function (req, res, next) {
        Category.find(function (err, categories) {
            if (err) {
                return next(new Errors.Mongo.FindError(err.message, err.status));
            }
            res.json(categories);
        });
    };

    /**
     * Возвращает данные найденной записи
     * по _id
     * @param req
     * @param res
     * @param next
     */
    CategoryController.prototype.getCategory = function (req, res, next) {
        var id = sanitizer.sanitize(req.params.id);
        if (!id) {
            next(new Errors.Error('Id parameter is not provided'));
        }
        Category.findById(id, function (err, category) {
            if (err) {
                return next(new Errors.Mongo.FindError(err.message, err.status));
            }
            res.json(category);
        });
    };

    /**
     * Сохраняет новый документ категории
     * отдает результат сохранения
     * @param req
     * @param res
     * @param next
     */
    CategoryController.prototype.saveNewCategory = function (req, res, next) {
        var validImages = [];
        if (!req.body.title) {
            return next(new Errors.Error('Category title is required'));
        }
        var title = sanitizer.sanitize(req.body.title);
        var images = req.files.images;
        if (images instanceof Array) {
            //noinspection JSPotentiallyInvalidUsageOfThis
            validImages = images.filter(this._isValidImage);
        } else {
            //noinspection JSPotentiallyInvalidUsageOfThis
            if (this._isValidImage) {
                validImages.push(images);
            }
        }

        var category = new Category({
            title: title,
            images: validImages
        });
        category.save(function (err, category) {
            if (err) {
                return next(new Errors.Mongo.SaveError(err.message, err.status));
            }
            res.json(category);
        });
    };

    /**
     * Изменяет документ категории
     * отдает результат обновления
     * @param req
     * @param res
     * @param next
     */
    CategoryController.prototype.updateCategory = function (req, res, next) {
        var title = sanitizer.sanitize(req.body.title);
        var images = req.files.images;

        if (!title) {
            next(new Errors.Error('Category title is required'));
        }

        var validImages = images.filter(function (element) {
            return imagesTypes.indexOf(element.mimetype) > -1;
        });

        var category = new Category({
            title: title,
            images: validImages
        });
        category.save(function (err, category) {
            if (err) {
                return next(new Errors.Mongo.SaveError(err.message, err.status));
            }
            res.json(category);
        });
    };

    CategoryController.prototype._isValidImage = function (image) {
        return imagesTypes.indexOf(image.mimetype) > -1;
    };

    CategoryController.prototype.patchCategory = function (req, res, next) {
        //console.log(req.json);
        res.json(req.body);
    };

    return CategoryController;
};

module.exports = CategoryController;