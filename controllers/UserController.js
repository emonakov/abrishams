var UserController = function (app) {
    'use strict';
    var EventEmitter = require("events").EventEmitter;
    var util = require("util");
    var jwt = require('jsonwebtoken');
    var sanitizer = require("sanitizer");
    var bcrypt = require('bcrypt-nodejs');


    var User = require('../models/UserModel');
    var Errors = require('../helpers/errors');

    var emitter = new EventEmitter();


    function UserController() {
        app.get('lodash').bindAll(this);
    }

    /**
     * Создает нового пользователя в базе
     * @param req
     * @param res
     * @param next
     */
    UserController.prototype.setupUser = function (req, res, next) {
        // create a sample user
        if (!req.body.name && !req.body.password) {
            return next(new Errors.Error('provide cridentials for the user', 403));
        }
        var name = sanitizer.sanitize(req.body.name);
        var password = sanitizer.sanitize(req.body.password);
        // Generate password hash
        var salt = bcrypt.genSaltSync();
        var password_hash = bcrypt.hashSync(password, salt);

        // Create user document
        var user = new User({'name': name, 'password': password_hash, admin: true});
        User.findOne({name: user.name}, function (err, doc) {
            if (err) {
                return next(new Errors.Mongo.FindError(err.message, err.status, err));
            }
            emitter.emit('user.new', !doc);
        });

        emitter.on('user.new', function (isNew) {
            if (isNew) {
                // save the sample user
                user.save(function (err, saved) {
                    if (err) {
                        return next(new Errors.Mongo.SaveError(err.message, err.status, err));
                    }
                    res.json(saved);
                });
            } else {
                return next(new Errors.Error('User already exists'));
            }
        });
    };

    /**
     * Возвращает данные пользователей
     * @param req
     * @param res
     * @param next
     */
    UserController.prototype.getUsers = function (req, res, next) {
        User.find({}, {password:0, __v:0}, function (err, users) {
            if (err) {
                next(new Errors.Mongo.FindError(err.message, err.status, err));
                return;
            }
            res.json(users);
        });
    };

    /**
     * Создать токен авторизации
     * @param req
     * @param res
     * @param next
     */
    UserController.prototype.authenticate = function (req, res, next) {
        var username = req.body.name;
        User.findOne({name: username}, function (err, user) {
            if (err) {
                return next(new Errors.Mongo.FindError(err.message, err.status, err));
            }
            emitter.emit('user.found', user);
        });
        emitter.on('user.found', function (user) {
            if (!user) {
                return next(new Errors.AuthError('user not found'));
            }
            if (!bcrypt.compareSync(req.body.password, user.password)) {
                return next(new Errors.AuthError('bad cridentials'));
            }
            // if user is found and password is right
            // create a token
            try {
                var token = jwt.sign(user, app.get('authKey'), {
                    expiresInMinutes: 1440 // expires in 24 hours
                });

                // return the information including token as JSON
                res.json({
                    token: token
                });
            } catch (err) {
                next(new Errors.Error(err.message, 500, err));
            }
        });
    };

    UserController.prototype.verifyWebToken = function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, app.get('authKey'), function (err, decoded) {
                if (err) {
                    return next(new Errors.TokenAuthError(err.message, 403, err));
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    return next();
                }
            });
        } else {
            return next(new Errors.TokenAuthError('No token provided.'));
        }
    };

    return UserController;
};

module.exports = UserController;