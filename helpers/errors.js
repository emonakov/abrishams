var ErrorsHelper = (function () {
    'use strict';
    var util = require("util");

    function TokenAuthError (message, status, parent) {
        Error.apply(this, arguments);
        Error.captureStackTrace(this, this.constructor);
        this.message = message;
        if (status) {
            this.status = status;
        }
        if(parent) {
            this.parent = parent;
        }
        Error.captureStackTrace(this, TokenAuthError);
    }
    util.inherits(TokenAuthError, Error);
    TokenAuthError.prototype.name = 'TokenAuthError';
    TokenAuthError.prototype.message = null;
    TokenAuthError.prototype.status = 403;
    TokenAuthError.prototype.parent = null;

    function CustomError(message, status, parent) {
        Error.apply(this, arguments);
        Error.captureStackTrace(this, this.constructor);
        this.message = message;
        this.status = status || 500;
        if(parent) {
            this.parent = parent;
        }
        Error.captureStackTrace(this, CustomError);
    }
    util.inherits(CustomError, Error);
    TokenAuthError.prototype.name = 'CustomError';
    CustomError.prototype.message = null;
    CustomError.prototype.status = null;
    CustomError.prototype.parent = null;

    function AuthError() {
        TokenAuthError.apply(this, arguments);
    }

    function MongoError() {
        CustomError.apply(this, arguments);
    }

    function MongoFindError() {
        MongoError.apply(this, arguments);
    }

    function MongoUpdateError() {
        MongoError.apply(this, arguments);
    }

    function MongoSaveError() {
        MongoError.apply(this, arguments);
    }

    util.inherits(AuthError, TokenAuthError);
    util.inherits(MongoError, CustomError);
    util.inherits(MongoFindError, MongoError);
    util.inherits(MongoUpdateError, MongoError);
    util.inherits(MongoSaveError, MongoError);
    AuthError.prototype.name = 'AuthError';
    MongoError.prototype.name = 'MongoError';
    MongoFindError.prototype.name = 'MongoFindError';
    MongoUpdateError.prototype.name = 'MongoUpdateError';
    MongoSaveError.prototype.name = 'MongoSaveError';

    return {
        TokenAuthError: TokenAuthError,
        Error: CustomError,
        AuthError: AuthError,
        Mongo: {
            Error: MongoError,
            FindError: MongoFindError,
            SaveError: MongoSaveError,
            UpdateError: MongoUpdateError
        }
    }
})();

module.exports = ErrorsHelper;