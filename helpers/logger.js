var morgan = require('morgan');
//var Logger = (function () {
//    'use strict';
//    var rotator = require('file-stream-rotator');
//    var fs = require('fs');
//    var morgan = require('morgan');
//
//    var logDirectory = __dirname + '/../logs';
//    // ensure log directory exists
//    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
//
//    // create a rotating write stream
//    var accessLogStream = rotator.getStream({filename: logDirectory + '/access-%DATE%.log', frequency: 'daily', verbose:true});
//
//    return morgan('combined', {stream: accessLogStream});
//})();
//module.exports = Logger; //Раскомментировать для прода
var winston = (function () {
    var expressWinston = require('express-winston');
    var winston = require('winston');
    return expressWinston.errorLogger({
        transports: [
            new winston.transports.File({
                //colorize: true
                filename: __dirname + '/logs/error.log',
                level: 'error',
                maxsize: 16777216,
                maxFiles: 5
            })
        ]
    });
})();

/**
 * Обработчик ошибоак для dev окружения
 * @param err
 * @param req
 * @param res
 * @param next
 */
var devErrorCallback = function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
};

/**
 * Обработчик ошибок для prod окружения
 * @param err
 * @param req
 * @param res
 * @param next
 */
var prodErrorCallback = function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({'error': err.message, full: err});
    //res.render('error', {
    //  message: err.message,
    //  error: {}
    //});
};

module.exports = {
    httpLogger: morgan,
    errorLogger: winston,
    devErrorCallback: devErrorCallback,
    prodErrorCallback: prodErrorCallback
};