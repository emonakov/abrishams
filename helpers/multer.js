'use strict';
var fs = require("fs");
module.exports = function (options) {
    var uploadDir = __dirname + '/../upload' || options.uploadDir;
    var limitFileSize = 2000000 || options.limitFileSize;
    var onUploadStart = null || options.onUploadStart;

    return require('multer')({
        dest: uploadDir,
        onFileUploadStart: onUploadStart,
        onFileSizeLimit: function (file) {
            fs.unlink(file.path);
        },
        limits: {
            fileSize: limitFileSize
        }
    });
};