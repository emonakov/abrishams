var CategoryModel = (function () {
    'use strict';
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var CategorySchema = new Schema({
        title:  String,
        images: Array,
        metaSchema: {}
    });

    return mongoose.model('Category', CategorySchema);
})();

module.exports = CategoryModel;