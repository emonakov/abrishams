var UserModel = (function () {
    'use strict';
    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var userSchema = new Schema({
        name: {type: String, unique: true},
        password: String,
        admin: Boolean
    });

    return mongoose.model('User', userSchema)
})();

module.exports = UserModel;