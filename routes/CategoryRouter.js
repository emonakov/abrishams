var CategoryRouter = function (app) {
    'use strict';
    var express = require('express');
    var fs = require("fs");
    var CategoryController = require('../controllers/CategoryController')(app),
        Controller =  new CategoryController();

    var multer = app.get('multer')({
        onUploadStart: function (file, req, res) {
            if(!Controller._isValidImage(file)){
                console.log(file);
                fs.unlink(file.path);
            }
        }
    });

    app.set('categoryController', Controller);
    var userController = app.get('userController');

    function CategoryRouter () {
        var router = express.Router();
        router.route('/').get(Controller.getAllCategories);
        router.use(userController.verifyWebToken);
        router.use(multer);
        router.route('/').post(Controller.saveNewCategory);

        router.route('/:id').get(Controller.getCategory);
        router.use(userController.verifyWebToken);
        router.use(multer);
        router.route('/:id').put(Controller.updateCategory);
        router.route('/:id').patch(Controller.patchCategory);
        return router;
    }
    return CategoryRouter();
};


module.exports = CategoryRouter;