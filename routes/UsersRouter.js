var UsersRouter = function (app) {
    'use strict';
    var express = require('express');
    var UserController = require("../controllers/UserController")(app);
    var userController = new UserController(app);
    app.set('userController', userController);

    function UsersRouter() {
        var router = express.Router();
        router.route('/auth')
            .post(userController.authenticate);
        router.use(userController.verifyWebToken);
        router
            .route('/')
            .get(userController.getUsers)
            .post(userController.setupUser);
        return router;
    }

    return UsersRouter();
};

module.exports = UsersRouter;
