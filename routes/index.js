var Router = function (app) {
   'use strict';
    var userRoutes = require('./UsersRouter')(app);
    var categoryRoutes = require('./CategoryRouter')(app);
    var auth = require("../helpers/auth")(app);

    //Мои роуты
    app.use('/api/users', userRoutes);
    app.use('/api/categories', categoryRoutes);
};

module.exports = Router;