var assert = require('assert');
var should = require("should");

describe("Category operations", function () {

    describe("create category", function () {
        describe("successfully crete category", function () {
            it("is authenticated");
            it("is able to save new category");
        });
    });

    describe("get category", function () {
        describe("successfully get category", function () {
            it("has a valid authkey");
            it("is able to retrieve category through mongoose model");
        });
    });
});